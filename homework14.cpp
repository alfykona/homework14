﻿#include <iostream>
#include <stdint.h>
#include <iomanip>
#include <string>

int main()
{
    std::cout << "Enter word: ";
    std::string word;
    std::getline(std::cin, word);

        std::cout << word << " " << "\n";
        std::cout << word.length() << "\n";
        std::cout << word[0] << "\n";
        std::cout << word.back();
}

